// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.theendcomplete.overtime.SecUser'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.theendcomplete.overtime.SecUserSecRole'
grails.plugin.springsecurity.authority.className = 'com.theendcomplete.overtime.SecRole'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
        [pattern: '/**', access: ['permitAll']],
        [pattern: '/error', access: ['permitAll']],
        [pattern: '/fonts/**', access: ['permitAll']],
        [pattern: '/index', access: ['permitAll']],
        [pattern: '/index.gsp', access: ['permitAll']],
        [pattern: '/shutdown', access: ['permitAll']],
        [pattern: '/assets/**', access: ['permitAll']],
        [pattern: '/**/js/**', access: ['permitAll']],
        [pattern: '/**/css/**', access: ['permitAll']],
        [pattern: '/**/images/**', access: ['permitAll']],
        [pattern: '/**/fonts/**', access: ['permitAll']],
        [pattern: '/screen/*', access: ['permitAll']],
        [pattern: '/secUser/**', access: ['permitAll']],
        [pattern: '/serviceNote/**', access: ['ROLE_COMMON']],

        [pattern: '/department/**', access: ['permitAll']],
        [pattern: '/worker/**', access: ['permitAll']],
        [pattern: '/**/favicon.ico', access: ['permitAll']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
        [pattern: '/assets/**', filters: 'none'],
        [pattern: '/**/js/**', filters: 'none'],
        [pattern: '/**/css/**', filters: 'none'],
        [pattern: '/**/fonts/**', filters: 'none'],
        [pattern: '/**/images/**', filters: 'none'],
        [pattern: '/**/favicon.ico', filters: 'none'],
        [pattern: '/**', filters: 'JOINED_FILTERS']
]

