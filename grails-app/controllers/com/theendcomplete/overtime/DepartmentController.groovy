package com.theendcomplete.overtime

class DepartmentController {
    static scaffold = Department

//    def index() { }
    def returnDepList = {
        def department = Department.get(params.department.id)
        render(template: 'department', model: [department: department.workers])
    }
}
