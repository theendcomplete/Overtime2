package com.theendcomplete.overtime

import grails.transaction.Transactional

@Transactional(readOnly = true)
class ServiceNoteController {
    def springSecurityService
    def serviceNoteService
    static scaffold = ServiceNote
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def createServiceNoteByUser = {
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        SecUser user = springSecurityService.currentUser
        def serviceNotes = user?.serviceNotes
        [user: user, serviceNoteList: serviceNotes, workers: Worker.list()]
        respond ServiceNote.list(params), model: [serviceNoteCount: ServiceNote.count()]
        redirect(action: 'createServiceNoteByUser')
    }

    def addServiceNoteByService() {
        println(params)
        def newServiceNote = serviceNoteService.createServiceNote(Department.get(params.department.id), new Date(), new Date(), params.reason, params.resolution)
        flash.message = "Added new serviceNote: ${newServiceNote.reason}"
        redirect(action: 'index')
//        newServiceNote.save()
    }

    def createServiceNoteByUser() {
        def max = 10
        params.max = Math.min(max ?: 10, 100)
        SecUser user = springSecurityService.currentUser
        def serviceNotes = user?.serviceNotes
        [user: user, serviceNoteList: serviceNotes, workers: Worker.list()]
        render(view: "createServiceNoteByUser", model: [sortedListUser:  user?.serviceNotes])
        respond ServiceNote.list(params), model: [serviceNoteCount: ServiceNote.count()]
    }
}


