package com.theendcomplete.overtime

class WorkerController {
    static scaffold = Worker
//    def index() { }

    def returnWorkersByDepartment = {
        params.name.each{i->
            System.out.println(i);
        }


        Department department = Department.get(params.departmentId)
        def workers = []
        if (department != null) {
            workers = department.workers
        }
        [workers: workers]
        render g.select(id: 'worker', name: 'worker.id',
                from: workers, optionKey: 'id', noSelection: [null: ' ']
        )
    }
}
