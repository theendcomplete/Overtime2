package com.theendcomplete.overtime

class Department {
    String name
    SecUser mainUser
    static hasMany = [workers: Worker]
    static constraints = {
        name(nullable: false)
        mainUser(nullable: true)
    }
    String toString() {
        return name
    }
}
