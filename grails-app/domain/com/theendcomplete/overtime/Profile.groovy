package com.theendcomplete.overtime

class Profile {
    static belongsTo = SecUser
    String fullName
    String occupation
    String email


    static constraints = {
        fullName(nullable: true)
        occupation(nullable: true)
        email(nullable: true)
    }

    String toString() {
        "Profile for ${fullName} (${id})"
    }
}
