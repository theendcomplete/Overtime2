package com.theendcomplete.overtime

class ServiceNote {
    Department department
    Date dateCreated
    Date startWork
    Date endWork
    String reason
    String resolution
    static belongsTo = [SecUser]
    static constraints = {
        department(nullable: true)
        dateCreated(nullable: true)
        startWork(nullable: true)
        endWork(nullable: true)
        reason(nullable: true)
        resolution(nullable: true)
    }
}
