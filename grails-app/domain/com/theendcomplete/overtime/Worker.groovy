package com.theendcomplete.overtime

class Worker {
    String name
    static belongsTo = [department: Department]
    static hasMany = [serviceNotes: ServiceNote]
    static constraints = {
        name(nullable: false)
        serviceNotes (nullable: true)
    }

    String toString() {
        return name
    }
}
