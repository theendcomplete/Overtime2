package overtime

import com.theendcomplete.overtime.Profile
import com.theendcomplete.overtime.SecRole
import com.theendcomplete.overtime.SecUser
import com.theendcomplete.overtime.SecUserSecRole

class BootStrap {

    def init = { servletContext ->
        if (!SecUser.findByUsername("admin")) {
            System.out.println("creating admin")
            SecUser admin = new SecUser(username: 'admin', password: 'secret', enabled: true).save()
            SecUser john = new SecUser(username: 'john', password: 'secret', enabled: true).save()
            SecUser jane = new SecUser(username: 'jane', password: 'secret', enabled: true).save()

//        Profile adminProf = new Profile(fullName: 'Admin', occupation: 'secret').save()
            admin.profile = new Profile(fullName: 'Admin', occupation: 'secret').save()
            admin.save()

//        String fullName
//        String occupation
//        String email

            SecRole royalty = new SecRole(authority: 'ROLE_ROYALTY').save()
            SecRole common = new SecRole(authority: 'ROLE_COMMON').save()
            SecUserSecRole.create(admin, royalty)
            SecUserSecRole.create(admin, common)
            SecUserSecRole.create(john, common)
        } else {
            System.out.println("already have admin")
        }

    }
    def destroy = {
    }
}
