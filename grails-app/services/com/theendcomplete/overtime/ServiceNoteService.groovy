package com.theendcomplete.overtime

import grails.transaction.Transactional

class ServiceNoteException extends RuntimeException {
    String message
    ServiceNote serviceNote
}


@Transactional
class ServiceNoteService {
    def springSecurityService
    def int MAX_ENTRIES_PER_PAGE = 10


    ServiceNote createServiceNote(Department department, Date startWork,
                                  Date endWork, String reason, String resolution) {
        SecUser user = springSecurityService.currentUser
        if (user) {
            def serviceNote = new ServiceNote(department: department, startWork: startWork, endWork: endWork, reason: reason).save()
            user.addToServiceNotes(serviceNote)
            user.save()

            if (user.save()) {
                return serviceNote
            } else {
                throw new ServiceNoteException(message: "invalid or empty sn")
            }

        } else {
            throw new ServiceNoteException(message: "invalid or empty user")

        }

    }

    def serviceMethod() {

    }
}
