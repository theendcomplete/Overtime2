<!DOCTYPE html>
<html>
<head>
    <script src="https://use.fontawesome.com/29a01f65c0.js"></script>
    <title><g:layoutTitle default="Grails"/></title>
    <asset:stylesheet src="application.css"/>
    <g:form controller="logout">
        <g:submitButton name="logout" value="Logout" />
    </g:form>
    <g:layoutHead/>
</head>
<body>
<g:layoutBody/>
<asset:javascript src="application.js"/>
</body>
</html>