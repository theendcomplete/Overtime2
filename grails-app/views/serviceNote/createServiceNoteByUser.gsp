<%@ page import="org.springframework.validation.FieldError; com.theendcomplete.overtime.Department; com.theendcomplete.overtime.Worker" %>
<!DOCTYPE html>
<html>
<head>

    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'serviceNote.label', default: 'ServiceNote')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
    <g:javascript library='jquery'/>
</head>

<body>
<a href="#create-serviceNote" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                    default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="create-serviceNote" class="content scaffold-create" role="main">
%{--<h1><g:message code="default.create.label" args="[entityName]"/></h1>--}%
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.serviceNote}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.serviceNote}" var="error">
                <li <g:if test="${error in FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>

%{--<g:form action="createServiceNoteByUser">--}%
    <form class="form-horizontal" action="addServiceNoteByService">
        <div id="container" class="form-horizontal">
            <legend>Создать служебную записку</legend>
            <br><br><br>

            <div class="form-group">
                <label class="col-md-4 control-label" for="department">Выберите Участок</label>
                <g:select id="department" name="department.id" from="${Department.listOrderByName()}"
                          optionKey="id"
                          values="${Department.list().name}"
                          noSelection="[null: 'Выберите что-то']"
                          onchange="categoryChanged(this.value);"/>
            </div>

            <!-- Select Multiple -->
            <div class="form-group">
                <label class="col-md-8 control-label" for="workers">Выберите сотрудников <br>
                    (для выбора нескольких зажмите клавишу ctrl)</label>

                <div class="col-md-8">
                    <g:select from="${workers}" name="workers" multiple="${true}"/>
                </div>
            </div>


            %{--datepicker--}%
            <div class="container">
                <div class='col-md-5'>
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker6'>
                            <input type='text' class="form-control"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class='col-md-5'>
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker7'>
                            <input type='text' class="form-control"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            %{--<dt>Производственная необходимость:</dt>--}%
            <div class="form-group">
                <label class="col-md-4 control-label" for="reason">Производственная необходимость:</label>

                <div class="col-md-4">
                    <textarea class="form-control" id="reason" name="reason" spellcheck="true"></textarea>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="btnSave"></label>

                <div class="col-md-4">
                    <dd><g:submitButton name="btnSave" class="btn btn-primary" value="Сохранить"/></dd>
                </div>
            </div>
        </div>
    </form>
    %{--</g:form>--}%
    <script>
        function categoryChanged(departmentId) {
            jQuery.ajax({
                type: 'POST',
                data: 'departmentId=' + departmentId,
                url: 'http://localhost:8080/worker/returnWorkersByDepartment',
                success: function (data, textStatus) {
                    jQuery('#workers').html(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
    </script>




    <div class="container">
        <div class="row">
            <div class='col-sm-6'>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(function () {
                    $('#datetimepicker1').datetimepicker();
                });
            </script>
        </div>
    </div>



    <script type="text/javascript">
        $(function () {
            $('#datetimepicker6').datetimepicker();
            $('#datetimepicker7').datetimepicker({
                useCurrent: false //Important! See issue #1075
            });
            $("#datetimepicker6").on("dp.change", function (e) {
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>


    <div class="container" id="table">
        <div class="row">
            <div class="span5">
                <table class="table table-striped table-condensed">
                    <thead>
                    <tr>
                        <th>Username</th>
                        <th>Date registered</th>
                        <th>Role</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Donna R. Folse
                            ${user}</td>
                        <td>2012/05/06</td>
                        <td>Editor</td>
                        <td><span class="label label-success">Active</span>
                        </td>
                    </tr>
                    <tr>
                        <td>Emily F. Burns</td>
                        <td>2011/12/01</td>
                        <td>Staff</td>
                        <td><span class="label label-important">Banned</span></td>
                    </tr>
                    <tr>
                        <td>Andrew A. Stout</td>
                        <td>2010/08/21</td>
                        <td>User</td>
                        <td><span class="label">Inactive</span></td>
                    </tr>
                    <tr>
                        <td>Mary M. Bryan</td>
                        <td>2009/04/11</td>
                        <td>Editor</td>
                        <td><span class="label label-warning">Pending</span></td>
                    </tr>
                    <tr>
                        <td>Mary A. Lewis</td>
                        <td>2007/02/01</td>
                        <td>Staff</td>
                        <td><span class="label label-success">Active</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</body>
</html>
